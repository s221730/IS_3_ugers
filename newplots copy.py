import pandas as pd
import matplotlib.pyplot as plt
df = pd.read_csv("empty.txt")
numbers = df.to_numpy()
træning = numbers[:round(len(numbers)*0.80)]
test = numbers[round(len(numbers)*0.80):]

gender = numbers[:,5]
sentence = numbers[:,11]
womansentence = []
mansentence = []
men = 0
women = 0

for x in range(len(gender)):
    if gender[x] == 0:
        men += 1
        mansentence.append(sentence[x])
    elif gender[x] == 1:
        women += 1
        womansentence.append(sentence[x])

antal = 50
manlisten = []
for i in range(antal):
    manlisten.append(0)
    for x in mansentence:
        if x >= i*6 and x < i*6+6:
            manlisten[i] += 1

womanlisten = []          
for i in range(antal):
    womanlisten.append(0)
    for x in womansentence:
        if x >= i*6 and x < i*6+6:
            womanlisten[i] += men/women

months = []
for i in range(int(antal/2)):
    string = ""
    string += str(i*12)
    string += "-"
    string += str(i*12+12)
    months.append(string)


xticksfake = [i*2+1 for i in range(int(antal/2))]
xticks = [i+1 for i in range(antal)]
fig, (ax2) = plt.subplots(1, 1)
ax2.set_xlabel("Months")
ax2.set_ylabel("Occurences")
ax2.set_xticks(xticksfake,months,rotation=70)
ax2.bar(xticks,manlisten,label = "Men",color="blue",alpha=0.5)
ax2.bar(xticks,womanlisten,label = "Women",color="red",alpha=0.5)
ax2.legend(loc="upper center", bbox_to_anchor=(0.75,1),ncol=3, fancybox=True, shadow=True)
ax2.set_title("Gender Sentence length distribution")
room2 = ax2.get_position() 
ax2.set_position([room2.x0, room2.y0 + room2.height * 0.15,room2.width, room2.height * 0.85])
plt.show()
