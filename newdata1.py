import pandas as pd
import numpy as np
file = pd.read_csv('empty.txt')
file = file.to_numpy()
newfile = open('fixeddata.csv', 'w')
newfile.write('')
for line in file:
    age = np.array(line[0])
    citizen = np.zeros(5)
    citizen[int(line[1])-1] = 1
    education = np.zeros(38)
    education[int(line[2])-1] = 1
    hispanic = np.zeros(3)
    hispanic[int(line[3])-1] = 1
    race = np.zeros(10)
    race[int(line[4])-1] = 1
    gender = np.array(line[int(5)])
    casetype = np.array(line[int(7)]-1)
    criminalhistory = np.array(line[int(8)])
    primarytypecrime = np.zeros(30)
    primarytypecrime[int(line[9])-1] = 1
    finaloffencelevel = np.array(line[int(10)])
    sentence = np.array(line[int(11)])
    newline  = np.append(age, citizen)
    newline  = np.append(newline, education)
    newline  = np.append(newline, hispanic)
    newline = np.append(newline, race)
    newline = np.append(newline, gender)
    newline = np.append(newline, casetype)
    newline = np.append(newline, criminalhistory)
    newline = np.append(newline, primarytypecrime)
    newline = np.append(newline, finaloffencelevel)
    newline = np.append(newline, sentence)
    mystring = ""
    for x in range(newline.size):
        if x > 0:
            mystring  = mystring + "," + str(newline[x])
        else:
            mystring = str(newline[x])
    newfile.write(mystring + "\n")
