import pandas as pd
import matplotlib.pyplot as plt
df = pd.read_csv("empty.txt")
numbers = df.to_numpy()
træning = numbers[:round(len(numbers)*0.80)]
test = numbers[round(len(numbers)*0.80):]

hispanic = test[:,3]
race = test[:,4]

noinfohispanic = 0
nonhispanic = 0
hispanicc = 0

whitecaucasian = 0
blackafricanamerican = 0
americanindianalaskan = 0
asianpacificislander = 0
multiracial = 0
other = 0
noinforace = 0
nonusamericanindian = 0
americanindiancitizenunknown = 0

for x in hispanic:
    if x == 0:
        noinfohispanic += 1
    elif x == 1:
        nonhispanic += 1
    elif x == 2:
        hispanicc += 1

for x in race:
    if x == 1:
        whitecaucasian += 1
    if x == 2:
        blackafricanamerican += 1
    if x == 3:
        americanindianalaskan += 1
    if x == 4:
        asianpacificislander += 1
    if x == 5:
        multiracial += 1
    if x == 7:
        other += 1
    if x == 8:
        noinforace += 1
    if x == 9:
        nonusamericanindian += 1
    if x == 10:
        americanindiancitizenunknown += 1

plt.bar([1,2,3],[noinfohispanic,nonhispanic,hispanicc],tick_label=["No Info","Non-Hispanic","Hispanic"])
plt.title("Hispanic Distribution (Test data)")
plt.show()

xticks = [1,2,3,4,5,6,7,8,9]
all = [whitecaucasian,blackafricanamerican,americanindianalaskan,asianpacificislander,multiracial,other,noinforace,nonusamericanindian,americanindiancitizenunknown]
fig, (ax2) = plt.subplots(1, 1)
ax2.set_xticks(xticks,["Caucasian","AfriAmer","AmerIndian/Alaskan","Asian/PacificIs","Multiracial","Other","No Info","NonUS AmerIndian","AmerIndian Citizen unknown"],rotation=20)
ax2.bar(xticks,all)
ax2.set_title("Race Distribution (Test data)")
room2 = ax2.get_position() 
ax2.set_position([room2.x0, room2.y0 + room2.height * 0.15,room2.width, room2.height * 0.85])
plt.show()
