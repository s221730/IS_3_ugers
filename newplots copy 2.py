import pandas as pd
import matplotlib.pyplot as plt
df = pd.read_csv("empty.txt")
numbers = df.to_numpy()

race = numbers[:,4]
sentence = numbers[:,11]

whitesentence = []
blacksentence = []

for x in range(len(sentence)):
    if sentence[x] < 800:
        if race[x] == 1:
            whitesentence.append(sentence[x])
        if race[x] == 2:
            blacksentence.append(sentence[x])

plt.boxplot([whitesentence,blacksentence])
plt.show()