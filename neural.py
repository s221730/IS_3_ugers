# %%
import torch
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import os

df = pd.read_csv("fixeddata.csv",header=None)
#print(df.head())
fil = df.to_numpy()
device = torch.device('cpu')

#seperate data into training and test
input = torch.tensor(fil[0:int(0.8*fil.shape[0]),0:len(fil[0])-1] , dtype=torch.float32, device = device)
#output = torch.tensor(np.log10(fil[0:int(0.8*fil.shape[0]),len(fil[0])-1]+ 1e-5) , dtype=torch.float32, device = device)
output = torch.tensor([math.log(x + 1e-5,10) for x in fil[0:int(0.8*fil.shape[0]),len(fil[0])-1]] , dtype=torch.float32, device = device)
test_input = torch.tensor(fil[int(0.8*fil.shape[0]):,0:len(fil[0])-1] , dtype=torch.float32, device = device)
test_output = torch.tensor(np.log10(fil[int(0.8*fil.shape[0]):,len(fil[0])-1]+ 1e-5) , dtype=torch.float32, device = device)
model = torch.nn.Sequential(
    torch.nn.Linear(len(fil[0])-1,30),
    torch.nn.ReLU(),
    torch.nn.Linear(30,30),
    torch.nn.ReLU(),
    torch.nn.Linear(30,30),
    torch.nn.ReLU(),
    torch.nn.Linear(30,1))

model.to(device)

loss = torch.nn.MSELoss(reduction = 'mean')

learning_rate = 0.01
n_iters = 500
losses  = np.zeros(n_iters)
test_losses = np.zeros(n_iters)
optimizer = torch.optim.Adam(model.parameters(), lr = learning_rate)
for epoch in range(n_iters):
    #torch.cuda.empty_cache()
    outpred = model(input)

    outpred = outpred.squeeze(-1)
    l = loss(outpred, output)
    losses[epoch] = l.item()
    test_losses[epoch] = loss(model(test_input).squeeze(-1),test_output).item()
    optimizer.zero_grad()
    l.backward()

    optimizer.step()

    if epoch % 100 == 0:
        print(losses[epoch])
        testerror  = loss(model(test_input).squeeze(-1), test_output)
        print(testerror.item())
# %%
list = [
26.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,13.0]
print(model(torch.tensor(np.array(list), dtype=torch.float32))) 
# %%
plt.hist(output)
# %%
plt.hist(torch.tensor([math.log(x + 1e-5,10) for x in fil[int(0.8*fil.shape[0]):,len(fil[0])-1]] , dtype=torch.float32, device = device))
# %%
np.mean(output.detach().numpy())
# %%
np.mean(np.array([math.log(x + 1e-5,10) for x in fil[int(0.8*fil.shape[0]):,len(fil[0])-1]]))
# %%
plt.hist(input[:,48])

# %%
plt.title("Gender distribution - training data")
plt.ylabel("Number of people")
plt.xticks((0,1), ("Men", "Women"))
plt.hist(input[:,57])
# %%
plt.title("Gender distribution - test data")
plt.ylabel("Number of people")
plt.xticks((0,1), ("Men", "Women"))
plt.hist(test_input[:,57])
# %%
plt.title("Age distribution - test data")
plt.ylabel("Number of people")
plt.xlabel("Age in years")
plt.hist(test_input[:,0])
# %%
plt.title("Age distribution - training data")
plt.ylabel("Number of people")
plt.xlabel("Age in years")
plt.hist(input[:,0])
# %%
plt.title("Estimation error")
plt.ylabel("Occurences")
plt.xlabel("log10 of error")
plt.hist(output.detach().numpy() - model(input).squeeze(-1).detach().numpy())
# %%
df.hist(column = "age", bins = 100)
# %%
print(len(input[0]))
# %%
actual = test_output.detach().numpy()
for i in range(len(actual)):
    actual[i] = 10**actual[i]-1e-5
pred = model(test_input).squeeze(-1).detach().numpy()
for i in range(len(pred)):
    pred[i] = 10**pred[i]-1e-5
zipped = list(zip(actual, pred))
acutalvspred = pd.DataFrame(zipped, columns = ["actual", "predicted"])
acutalvspred.plot(kind="scatter", x="actual", y="predicted")
plt.xlabel("Actual sentence in months")
plt.ylabel("Predicted sentence in months")
plt.title("Actual vs predicted")
plt.show()
# %%
acutalvspred = pd.DataFrame(zipped, columns = ["actual", "predicted"])
acutalvspred.plot(kind="scatter", x="actual", y="predicted", xlim = (0,600))
plt.xlabel("Actual sentence in months")
plt.ylabel("Predicted sentence in months")
plt.title("Actual vs predicted")
plt.show()
# %%
#plot loss curve
plt.plot(losses)
plt.plot(test_losses)
plt.title("Loss curve")
plt.xlabel("Epoch")
plt.ylabel("Loss")
plt.show()
# %%
plt.hist(actual, bins = 100)
plt.title("Sentence level distribution - Test")
plt.xlabel("Sentence length in months")
plt.ylabel("Occurences")
plt.show()

# %%





# %%
df = pd.read_csv("empty.txt")
numbers = df.to_numpy()
race = numbers[:,4]
sentence = numbers[:,11]

whitesentence = []
blacksentence = []

predwhitesentence = []
predblacksentence = []

predwhitelosssentence = []
predblacklosssentence = []


for x in range(len(sentence)):
    if sentence[x] < 1000:
        if race[x] == 1:
            whitesentence.append(sentence[x])
        if race[x] == 2:
            blacksentence.append(sentence[x])


predsentence = []
for x in range(len(numbers[:,11])-1):
    pred = 10**(model(torch.tensor(fil[x,0:len(fil[0])-1] , dtype=torch.float32, device = device)).detach().numpy()[0])
    
    predsentence.append(pred)

for x in range(len(predsentence)):
    if predsentence[x] < 1000:
        if race[x] == 1:
            predwhitesentence.append(predsentence[x])
            if abs(sentence[x]-predsentence[x]) < 1000:
                predwhitelosssentence.append(sentence[x]-predsentence[x])
        if race[x] == 2:
            predblacksentence.append(predsentence[x])
            if abs(sentence[x]-predsentence[x]) < 1000:
                predblacklosssentence.append(sentence[x]-predsentence[x])


# %%
df = pd.read_csv("empty.txt")
numbers = df.to_numpy()
removedsentence = []
removedwhitelosssentence = []
removedblacklosssentence = []

for x in range(len(numbers[:,11])-1):
    pred = 10**(model(torch.tensor(fil[x,0:len(fil[0])-1] , dtype=torch.float32, device = device)).detach().numpy()[0])
    removedsentence.append(pred)

removedwhitesentence = []
removedblacksentence = []

for x in range(len(removedsentence)):
    if removedsentence[x] < 1000:
        if race[x] == 1:
            removedwhitesentence.append(removedsentence[x])
            if abs(sentence[x]-removedsentence[x]) < 1000:
                removedwhitelosssentence.append(sentence[x]-removedsentence[x])
        if race[x] == 2:
            removedblacksentence.append(removedsentence[x])
            if abs(sentence[x]-removedsentence[x]) < 1000:
                removedblacklosssentence.append(sentence[x]-removedsentence[x])

#%%

# %%
plt.title("Sentence lengths with/without sensitive variable (gender) compared")
plt.ylabel("Sentence Length")
sentences = [whitesentence,blacksentence,predwhitesentence,predblacksentence,removedwhitesentence,removedblacksentence]
plt.xticks(rotation=20)
plt.boxplot(sentences,labels= ["W","B","PredW","PredB","RemovW","RemovB"])
plt.show()
plt.title("Sentence lengths with/without sensitive variable (gender) compared")
plt.ylabel("Sentence Loss")
sentences = [predwhitelosssentence,predblacklosssentence,removedwhitelosssentence,removedblacklosssentence]
plt.xticks(rotation=20)
plt.boxplot(sentences,labels= ["LossPredW","LossPredB","LossRemovW","LossRemovB"])
plt.show()
# %%

df = pd.read_csv("empty.txt")
numbers = df.to_numpy()
predsentence = []
print(len(numbers[:,11]))
print(len(fil[:,0:len(fil[0])-1]))
for x in range(len(numbers[:,11])):
    pred = 10**(model(torch.tensor(fil[x,0:len(fil[0])-1] , dtype=torch.float32, device = device)).detach().numpy()[0])
    predsentence.append(pred)


# %%
# sentence = numbers[:,11]
# zipsentence = list(zip(sentence,predsentence))
# newsentence = sorted(zipsentence, key=lambda x: x[0])
# print(newsentence[:10])
# xs = []
# acty = []
# predy = []
# for i in range(0,len(sentence)-100,50):
#     xs.append(i)
#     acty.append(np.mean(list(zip(*newsentence[i:i+50]))[0]))
#     predy.append(np.mean(list(zip(*newsentence[i:i+50]))[1]))

# plt.title("Predicted vs Actual Sentence Length")
# plt.scatter(xs,predy,label="Predicted Sentence Length")
# plt.scatter(xs,acty,label="Actual Sentence Length")
# plt.legend()
# plt.show()
# %%
# sentence = numbers[:,11]
# zipsentence = list(zip(sentence,predsentence))
# newsentence = sorted(zipsentence, key=lambda x: x[0])
# print(newsentence[:10])
# xs = []
# acty = []
# predy = []
# for i in range(0,len(sentence)-100,20):
#     xs.append(i)
#     acty.append(newsentence[i][0])
#     predy.append(newsentence[i][1])

# plt.title("Predicted vs Actual Sentence Length")
# plt.scatter(xs,predy,label="Predicted Sentence Length")
# plt.scatter(xs,acty,label="Actual Sentence Length")
# plt.legend()
# plt.show()
# %%
